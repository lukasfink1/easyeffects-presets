# ThinkPad X1 Yoga Gen2 Easy Effects preset

This Easy Effects preset tries to improve the sound of the ThinkPad X1 Yoga Gen2’s internal speakers.

It was created by following Markus Schmidt’s guide included in the help of Easy Effects.

The Makeup of Band 3 of the Multiband Compressor is a compromise. On a hard desk surface decreasing the value a bit might improve the sound, whereas on soft surfaces (e.g. the armrest of a sofa, a tablecloth, the user’s lap) increasing this value tended to improve the sound.

Input signals with high dynamic range (e.g. movies, classical music) can be made louder by increasing the input gain of the Multiband Compressor if the sound card’s output volume is already at maximum. This is especially useful with movies if the dialogues are hard to understand.

Overall, don’t be afraid of experimenting with the settings if you don’t like something about the sound.
